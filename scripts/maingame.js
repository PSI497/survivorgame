var canvas = document.getElementById("renderCanvas");
var engine = new BABYLON.Engine(canvas,true);
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

var character;
var down = false;
var up = false;
var right = false;
var left = false;
var createScene = function () {

    var scene = new BABYLON.Scene(engine);
    var camera = new BABYLON.FollowCamera("FollowCam",new BABYLON.Vector3(10,10,10),scene);
    camera.radius = 10; //goal distance of camera from target
    camera.heightOffset = 20;
    camera.noRotationConstraint = true;
    var spriteManagerTrees = new BABYLON.SpriteManager("treesManager", "textures/palm.png", 2000, 800, scene);

    //We create 2000 trees at random positions
    for (var i = 0; i < 2000; i++) {
        var tree = new BABYLON.Sprite("tree", spriteManagerTrees);
        tree.position.x = Math.random() * 100 - 50;
        tree.position.z = Math.random() * 100 - 50;
        tree.isPickable = true;
    }

    //Create a manager for the player's sprite animation
    var spriteManagerPlayer = new BABYLON.SpriteManager("playerManager", "textures/player.png", 2, 64, scene);

    // First animated player
    var player = new BABYLON.Sprite("player", spriteManagerPlayer);
    player.playAnimation(0, 40, true, 100);
    player.position.y = -0.3;
    player.size = 0.3;
    player.isPickable = true;

    // Second standing player
    var player2 = new BABYLON.Sprite("player2", spriteManagerPlayer);
    player2.stopAnimation(); // Not animated
    player2.cellIndex = 2; // Going to frame number 2
    player2.position.y = -0.3;
    player2.position.x = 1;
    player2.size = 0.3;
    player2.invertU = -1; //Change orientation
    player2.isPickable = true;


    // Picking
    spriteManagerTrees.isPickable = true;
    spriteManagerPlayer.isPickable = true;

    scene.onPointerDown = function (evt) {
        var pickResult = scene.pickSprite(this.pointerX, this.pointerY);
        if (pickResult.hit) {
            pickResult.pickedSprite.angle += 0.5;
        }
    }; //glao height of camera above local origin (centre) of target
    // The goal rotation of camera around local origin (centre) of target in x y plane
   // camera.rotationOffset = 0;

    //Acceleration of camera in moving from current to goal position
   // camera.cameraAcceleration = 0.005

    //The speed at which acceleration is halted 
   // camera.maxCameraSpeed = 10
    camera.attachControl(canvas, true);
    character  = BABYLON.MeshBuilder.CreateSphere("sphere", {diameter:2}, scene);
    camera.lockedTarget = character; 

    var light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(1, 1, 0), scene);
   // var light2 = new BABYLON.PointLight("light2", new BABYLON.Vector3(0, 1, -1), scene);
    var ground = BABYLON.Mesh.CreateGround("ground1", 200, 200, 0, scene);
    var spriteManagerPlayer = new BABYLON.SpriteManager("playerManagr","character.png", 2, 64, scene);
    var materialPlane = new BABYLON.StandardMaterial("texturePlane", scene);
    materialPlane.diffuseTexture = new BABYLON.Texture("grass.jpg", scene);
    materialPlane.diffuseTexture.uScale = 7.0;//Repeat 5 times on the Vertical Axes
    materialPlane.diffuseTexture.vScale = 7.0;//Repeat 5 times on the Horizontal Axes
    materialPlane.backFaceCulling = false;//Always show the front and the back of an element
    ground.material = materialPlane;
    return scene;
};
var scene = createScene(); //Call the createScene function

engine.runRenderLoop(function () { // Register a render loop to repeatedly render the scene
    if (down){
        character.position.z -= 0.1;
    }
    if(up){
        character.position.z += 0.1
    }
    if (left){
        character.position.x -= 0.1;
    }
    if(right){
        character.position.x += 0.1
    }

    scene.render();
});

window.addEventListener("resize", function () { // Watch for browser/canvas resize events
    engine.resize();
});

function keyDownHandler(e) {
    if (e.key === "w") {
        up = true;
    }
    if (e.key === "s") {
        down = true;
    }
    if (e.key === "d") {
        right = true;
    }
    if (e.key === "a") {
        left = true;
    }
}
function keyUpHandler(e) {
    if (e.key === "w") {
        up = false;
    }
    if (e.key === "s") {
        down = false;
    }
    if (e.key === "d") {
        right = false;
    }
    if (e.key === "a") {
        left = false;
    }
}